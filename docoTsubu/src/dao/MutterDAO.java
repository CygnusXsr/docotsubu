package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Mutter;

public class MutterDAO {
	 final String DRIVER_NAME = "com.mysql.jdbc.Driver";//MySQLドライバ
     final String DB_URL = "jdbc:mysql://localhost:3306/";//DBサーバー名
     final String DB_NAME = "example";//データベース名
     final String DB_ENCODE = "?useUnicode=true&characterEncoding=utf8";//文字化け防止
     final String DB_USER = "root";//ユーザーID
     final String DB_PASS = "root";//パスワード
     public List<Mutter> findAll(){
    	 Connection conn = null;
    	 List<Mutter> mutterList = new ArrayList<Mutter>();
    	 try {
			Class.forName(DRIVER_NAME);
			conn = DriverManager.getConnection(DB_URL + DB_NAME+DB_ENCODE , DB_USER, DB_PASS);
	    	String sql = "select id,name,text from mutter order by id desc";
	    	PreparedStatement pStmt = conn.prepareStatement(sql);
	    	ResultSet rs = pStmt.executeQuery();
	    	while(rs.next()){
	    		int id = rs.getInt("ID");
	    		String userName = rs.getString("NAME");
	    		String text = rs.getString("TEXT");
	    		Mutter mutter = new Mutter(id,userName,text);
	    		mutterList.add(mutter);
	    	}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			return null;
		} finally{
			if(conn != null){
				try{
					conn.close();
				} catch(SQLException e){
					e.printStackTrace();
					return null;
				}
			}
		}
    	 return mutterList;
     }

     public boolean create(Mutter mutter){
    	 Connection conn = null;
    	 try{
    		 conn = DriverManager.getConnection(DB_URL + DB_NAME+DB_ENCODE , DB_USER, DB_PASS);
    		 String sql = "insert into mutter(name,text) values(?,?)";
    		 PreparedStatement pStmt = conn.prepareStatement(sql);
    		 pStmt.setString(1, mutter.getUserName());
    		 pStmt.setString(2, mutter.getText());
    		 int result = pStmt.executeUpdate();
    		 if(result != 1){
    			 return false;
    		 }
    	 } catch(SQLException e){
    		 e.printStackTrace();
    	 } finally{
    		 if(conn != null){
    			 try{
    				 conn.close();
    			 } catch(SQLException e){
    				 e.printStackTrace();
    			 }
    		 }
    	 }
    	 return true;
     }

}
